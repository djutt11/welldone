'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var pricingTechnique = new Schema(
  {
    title : String,
    description: String,
    price : Number
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);



var PricingTechnique = mongoose.model('PricingTechnique', pricingTechnique );
module.exports = PricingTechnique;