'use strict'

var PricingTechnique = require('./pricing.technique.schema')


var persist = function (pricingTechnique ) {

  var pricingTechModel  = new PricingTechnique(pricingTechnique );
  return pricingTechModel.save();
}

var findById = function (id) {
  return PricingTechnique.findById(id).exec();
}

var search = function (params) {

}


var list = function () {
  return PricingTechnique.find().exec();
}


module.exports ={
  'persist' : persist,
  'findById' : findById,
  'search' : search,
  'list' : list
}
