/**
 * Created by damjad on 12/4/17.
 */
'use strict';

var express = require('express')

var pTechService = require('./orm/pricing.technique.service');

var appRoute = new express.Router();

/**
 *
 * @param container
 * @returns {*}
 */
module.exports = function (container) {

  appRoute.get('/',function (req,res) {
    var promise = pTechService.list();
    promise.then(function(reviews, err){
      if(err){
        res.send(container.response.error(err));
      }
      res.send(container.response.success(reviews));
    });

  });

  /**
   * POST Creates a new customer.
   */
  appRoute.post('', function(req, res, next){

    var pTechique={};

    pTechique.title=req.body.title || '';
    pTechique.description=req.body.description || '';
    pTechique.price=req.body.price || 0;

    var promise = pTechService.persist(pTechique);
    promise.then(function(customer, err){
      if(err) {
        res.status(500).send(err);
      } else {
        res.send(customer);
      }
    });
  });

  return appRoute;
}