'use strict';

var express = require('express');

var async = require('async');

var _ = require('lodash');
var appRoute = new express.Router();


module.exports=function (container) {


    var a = function(callback){

        setTimeout(function(){
            console.log('A Arguments ::', arguments);
            console.log("A is called");


            callback(null, 'A');
            console.log('AFTER CALLBACK AA-----------------');
            return "A";

        },1000);



    };
    var b = function(callback){
        console.log('B Arguments ::', arguments);
        console.log("B is called");
        callback(null, 'B');
        return "B";
    }

    var c = function(callback){
        console.log('C Arguments ::', arguments);
        console.log("C is called");
        if(callback){
            callback(null, "C");
        }
        return "C";
    }

    var d = (callback) => {

        console.log('D Arguments ::', arguments);
        console.log("D is called");
        if(callback){
            callback(null, "D");
        }
        return "D";
    };

    
    appRoute.get(['', '/p', '/parallel'],function (req,res) {
        /**
         *
         */
        async.parallel([a,d], function(error, results){
            console.log("RESULTS--->>>",results);
            if(error){
                res.send(container.response.error("This is an ERROR"));
                return;
            }
            var aResult = results[0];
            c(null);
            res.send(container.response.success('things execute parallel'));
        });
    });
    appRoute.get(['/s', '/series'],function (req,res) {
        /**
         *
         */
        async.series([a,b, c], function(error){
            console.log("Arguments--->>>",arguments);
            if(error){
                res.send(container.response.error("This is an ERROR"));
                return;
            }
            res.send(container.response.success('things execute parallel'));
        });
    });



    return appRoute;
}