'use strict';

var express = require('express');
var orderService = require('./orm/order.service');
var reviewService = require('../review/orm/review.service');
var async = require('async');

var _ = require('lodash');
var appRoute = new express.Router();

module.exports = function(container){
    appRoute.get('/', function(req, res){

        console.log('***QUERY***********', req.query);
        var params = {};

        if(!_.isUndefined(req.query.service)){
            params.service = req.query.service;
        }
        var promise = orderService.search(params);
        promise.then(function(orders, err){
            if(err){
                res.send(container.response.error(err));
                return;
            }
            res.send(container.response.success(orders));
        });
        /*
        let data = [
            'Careem', 'Uber', 'Travely', 'Rocket Rishah', 'Bikea'
        ];
        res.send(container.response.success(data));*/
    });
    
    appRoute.post('', function (req, res) {

        var order = {};
        order.customer = req.body.customer || null;
        order.service = req.body.service || null;
        order.contractor = req.body.contractor || null;
        order.notes = req.body.notes || null;
        if(!_.isUndefined(req.body.reviews)){
            order.reviews=req.body.reviews;
        }

        var p = orderService.persist(order);
        p.then(function (order) {
            res.send(container.response.success(order));
        });

    })

    /**
     *
     */
    appRoute.post('/:id/review', function(req, res){

        var review={};

        review.order = req.params.id || null;
        review.rating = req.body.rating || null;
        review.review = req.body.review || null;

        var promise = reviewService.persist(review);
        promise.then(function(review, err){
            if(err){
                res.send(container.response.error(err));
            }
            res.send(container.response.success(review))
        });
    });
    /*
    *  View order
    * */
    appRoute.get('/:id', function(req, res){


        console.log('order id ');

        var orderId= req.params.id || null;


        var promise = orderService.findById(orderId);
        promise.then(function(order, err){
            if(err){
                res.send(container.response.error(err));
            }
            res.send(container.response.success(order))
        });
    });

    /**
     *
     */
    appRoute.get('/:id/review', function(req, res){

    });



    appRoute.get('/:id/parallel',function (req,res) {


    })
    appRoute.get('/:id/series',function (req,res) {

    })


    return appRoute;
};