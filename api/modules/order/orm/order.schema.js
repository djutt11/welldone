'use strict';
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var Schema = mongoose.Schema;

var Reviews = new Schema(
    {
    review : String,
    rating : Number},
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

var orderSchema = new Schema({
    "customer": {
        type: Schema.Types.ObjectId,
        ref: 'Customer'
    },
    "service" : {
        type: Schema.Types.ObjectId,
        ref: 'Service'
    },
    "contractor" : {
        type: Schema.Types.ObjectId,
        ref: 'Contractor'
    },
    notes: String,
    cancelled : Boolean,
    reviews  : [Reviews]
},
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

orderSchema.plugin(mongoosePaginate);

var Order = mongoose.model('Order', orderSchema);
module.exports = Order;