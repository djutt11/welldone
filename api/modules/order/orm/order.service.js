'use strict';

var Order = require('./order.schema');

var persist = function(order){
    var orderEntity = new Order(order);
    orderEntity.cancelled = false;
    return orderEntity.save();
};

/**
 *
 * @param id
 * @returns {Promise}
 */
var findById = function(id){
    return Order.findById(id)
        .populate('service')
        .populate('customer')
        .exec();
}

/**
 *
 * @param params
 * @returns {Promise}
 */
var search = function(params){
    params = params || {};

    console.log('search Params', params);
    return Order.paginate({}, { page: 1, limit: 5,
        populate:['service','customer','contractor'],
        sort: { created_at: -1 }
    });
    return Order.find(params)
        .populate('service')
        .populate('customer')
        .populate('contractor').sort({'created_at':-1})
        .exec();
}

var list = function(){
    return Order.find().exec();
}


module.exports = {
    'persist' : persist,
    'findById' : findById,
    'search' : search,
    'list' : list
};
