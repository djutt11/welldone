/**
 * Created by damjad on 12/4/17.
 */
'use strict';

var express = require('express');

var dashboardService = require('./dashboard.service');


var appRoute = new express.Router();

/**
 *
 * @param container
 * @returns {*}
 */
module.exports = function (container) {

  appRoute.get('/admin/dashboard',function (req,res) {

    dashboardService.getService();

    res.render('dashboard/admin');
  });
  appRoute.get('/publisher/dashboard',function (req,res) {
    res.render('dashboard/publisher');
  });
  appRoute.get('/client/dashboard',function (req,res) {
    res.render('dashboard/client');
  });

  return appRoute;
}