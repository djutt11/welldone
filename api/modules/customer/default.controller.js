'use strict';

var express = require('express');
var customerRoute = new express.Router();
var customerService = require('./orm/customer.service');

module.exports = function(containers){

    customerRoute.get('', function(req, res){
        containers.logger.info("Runing some file.....");
        var promise = customerService.search({});
        promise.then(function(customers, err){
            if(err) {
                res.send(containers.response.error(400, err));
                return;
            }
            res.send(containers.response.success(customers));
        });

    });

    customerRoute.get('/:id', function(req, res){
        var id = req.param('id')|| null;
        var p = customerService.findById(id);
        p.then(function(customer, err){
            if(err) {
                res.status(500).send(err);
                return;
            }
            res.send({'My' : "Last", 'error' : err, '____id' : req.path.id});
        });
    });

    /**
     * POST Creates a new customer.
     */
    customerRoute.post('', function(req, res, next){
        var promise = customerService.persist(req.body);
        promise.then(function(customer, err){
            if(err) {
                res.status(500).send(err);
            } else {
                res.send(customer);
            }
        });
    });

    /**
     *
     */
    customerRoute.get('/:id/reviews', function(req, res){

    });

    return customerRoute;
}