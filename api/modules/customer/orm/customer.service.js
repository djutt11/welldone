'use strict';

var Customer = require('./customer.schema');

/**
 *
 * @param data
 */
var persist = function(data){
    var customer = new Customer(data);
    return customer.save();
};

/**
 *
 * @param id
 * @returns {Promise}
 */
var findById = function(id){
    var query = Customer.findById(id);
    return query.exec();
};

/**
 *
 * @param params
 */
var search = function(params){
    var query = Customer.find(params);
    return query.exec();
}

var update = function(){

};


var customerService = {
    'persist' : persist,
    'findById' : findById,
    'search' : search,
    'update' : update
};
module.exports = customerService;
