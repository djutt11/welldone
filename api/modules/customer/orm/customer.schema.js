'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var customerSchema = new Schema({
    firstName: String,
    lastName : String,
    email: String,
    mobile: String,
    lang : String,
},
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

/*customerSchema.pre('save', function(next){
    this.created_at = Date.now();
    next();
});

customerSchema.pre('update', function(){
    this.update({},{ $set: { updatedAt: new Date() } });
});*/

var Customer = mongoose.model('Customer', customerSchema);




// make this available to our users in our Node applications
module.exports = Customer;