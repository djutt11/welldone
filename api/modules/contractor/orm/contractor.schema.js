/**
 * Created by damjad on 12/5/17.
 */

'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var coverageSchema = new Schema({
    "type": {type: String},
    "coordinates": []
});

var noteSchema = new Schema({
    'note' : String
});

var contractorSchema = new Schema({
        service : [{
            type: Schema.Types.ObjectId,
            ref: 'Service'
        }],
        name: String,
        phone : String,
        notes: [noteSchema],
        home : coverageSchema,
        areas : [coverageSchema],
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

contractorSchema.index({ "areas": "2dsphere" });
contractorSchema.index({ "home_idx": "2dsphere" });
var Contractor = mongoose.model('Contractor', contractorSchema);
module.exports = Contractor;
