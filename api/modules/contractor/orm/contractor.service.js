/**
 * Created by damjad on 12/5/17.
 */
'use strict';

var Contractor = require('./contractor.schema');

/**
 *
 * @param contractor
 * @returns {Promise|*}
 */
var persist = function(contractor){
    var contractorEntity = new Contractor(contractor);
    return contractorEntity.save();
};

/**
 *
 * @param id
 * @returns {Promise}
 */
var findById = function(id){
    return Contractor.findById(id).exec();
}

/**
 *
 * @param params
 * @returns {Promise}
 */
var search = function(params){

    params = params || {};

    console.log('search Params', params);
    return Contractor.find(params)
        .populate('service')
        .exec();

}

/**
 *
 * @returns {Promise|Array|{index: number, input: string}|*}
 */
var list = function(){
    return Contractor.find().exec();
}

/**
 *
 * @param id
 * @param coords
 * @returns {*}
 */
var addCoverage = function (id, coords) {
    var polygon = {
        'type' : 'Polygon',
        'coordinates' : coords
    };
    return Contractor.update(
        {_id: id},
        {$push: {'areas': polygon}},
        {safe: true, upsert: true, new : true}
    ).exec();
};


var pushArea = function (id, coords) {
    var polygon = {
        'type' : 'Polygon',
        'coordinates' : coords
    };
    return Contractor.findOne({'_id' : id}, function(err, contractor){
        contractor.areas.push(polygon);
        return contractor.save();
    });
};

module.exports = {
    persist, findById, search, list, addCoverage, pushArea
};
