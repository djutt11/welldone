/**
 * Created by damjad on 12/5/17.
 */
'use strict';

var express = require('express');
var winston = require('winston');
var _ = require('lodash');

var contractorServcie= require('./orm/contractor.service');

var appRoute = new express.Router();

module.exports = function (container) {

    appRoute.post('', function (req, res) {
        var contractor = {};
        contractor.name=req.body.name;
        contractor.phone=req.body.phone;
        contractor.service=req.body.service || null;

        var p = contractorServcie.persist(contractor);
        p.then(function (contractor) {
            res.send(container.response.success(contractor));
        });

    })

    appRoute.get('/', function(req, res){

        console.log('----------------------------------------');
        winston.info('THIS IS LOCAL LOGGER >.. Prints only on console');
        container.logger.info('THIS IS GLOBAL AND SHOULD WRITE IN FILE AS WELL');

        var promise = contractorServcie.list();
        container.logger.log("Logger Working ???");
        promise.then(function(contractors, err){
            if(err){
                res.send(container.response.error(err));
                return;
            }
            res.send(container.response.success(contractors));
        });
    });


    appRoute.post('/:id/coverage', function(req, res){

        var id = req.params.id || null;
        var coords = req.body.coords || null;
        var promise = contractorServcie.addCoverage(id, coords);
        promise.then(function(contractor, err){
            if(err){
                res.send(container.response.error(err));
                return;
            }
            res.send(container.response.success(contractor));
        })
    });

    appRoute.get('/queue', function(req, res){



    });

    return appRoute;
}
