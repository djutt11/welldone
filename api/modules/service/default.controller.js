'use strict';

var express = require('express');
var sService = require('./orm/service.service');
var _ = require('lodash');
module.exports = function(container){
    var app = new express();

    app.get('', function (req, res) {
        var promise = sService.list();

        promise.then(function(services, err){
            if(err){
                res.send(container.response.error(err));
                return;
            }
            res.send(container.response.success(services));
        });



/*        p.then(function(err, services){
            if(err) res.status(500).send(err);
            res.send(services);
        });*/
    });

    /**
     *
     */
    app.post('', function (req, res) {
        console.log('POSTING SERVICE');
        var service = {};

        service.name = req.body.name || '';
        service.description = req.body.description || '';
        service.image_url = req.body.image_url || '';

      if(!_.isUndefined(req.body.pricing)){

          console.log('req.body.pricing',req.body.pricing);

        service.pricingTechnique=req.body.pricing;
        console.log('pt save obj',service.pricingTechnique);
      }


        var p = sService.persist(service);
        p.then(function(service, err){
            if(err) res.send(container.response.error(400, err.message));
            res.send(container.response.success(service));
        });
        p.catch(function(rejection){
            res.send(container.response.error(400, 'Fail to save service'));
        })
    });

    /**
     * 
     */
    app.get("/:id/reviews", function(req, res){

    });

    app.get("/:id/contractors", function(req, res){


        console.log('get contr.....');

        var sId= req.params.id || null;

        var promise = sService.contractors(sId);
        promise.then(function(contractors, err){
            if(err){
                res.send(container.response.error(err));
                return;
            }
            res.send(container.response.success(contractors));
        });

    });



    return app;
};