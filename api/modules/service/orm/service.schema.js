'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var PricingTechnique = new Schema(
  {
    title : String,
    description: String,
    price : Number
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);


var serviceSchema = new Schema(
    {
        name : String,
        description : String,
        imageUrl : String,
        pricingTechnique : [PricingTechnique]

    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

var Service = mongoose.model('Service', serviceSchema);
module.exports = Service;