'use strict';
var Service = require('./service.schema');

var Contractor = require('../../contractor/orm/contractor.schema');

const list = function(params){
    params = params || {};
    return Service.find(params).exec();
};

const persist = function(service){
    var serviceEntity = new Service(service);
    return serviceEntity.save();
};

const contractors =function (id) {
    var params = {'service' : id};
    return Contractor.find(params).exec();
};

/**
 *
 * @type {{list: list}}
 */
module.exports = {
    list:list,
    persist:persist,
    contractors:contractors
};