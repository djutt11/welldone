/**
 * Created by damjad on 12/4/17.
 */
'use strict';

var express = require('express')

var reviewService = require('./orm/review.service');

var appRoute = new express.Router();

/**
 *
 * @param container
 * @returns {*}
 */
module.exports = function (container) {

    appRoute.get('/',function (req,res) {
        var promise = reviewService.list();
        promise.then(function(reviews, err){
            if(err){
                res.send(container.response.error(err));
            }
            res.send(container.response.success(reviews));
        });

    });

    return appRoute;
}