/**
 * Created by damjad on 12/4/17.
 */
'use strict'

var Review = require('./review.schema')


var persist = function (review) {

    var reviewModel = new Review(review);
    return reviewModel.save();
}

var findById = function (id) {
    return Review.findById(id).exec();
}

var search = function (params) {

}


var list = function () {
    return Review.find().populate('order').exec();
}


module.exports ={
    'persist' : persist,
    'findById' : findById,
    'search' : search,
    'list' : list
}



