'use strict';

var mongoose = require('mongoose')
var Schema =mongoose.Schema;

var reviewSchema = new Schema(
    {
    "order" : {
        type: Schema.Types.ObjectId,
        ref: 'Order'
    },
    rating : Number,
    review : String
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

var Review = mongoose.model('Review',reviewSchema);

module.exports = Review;