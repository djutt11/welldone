'use strict';

var express = require('express');
module.exports = function(container){
    container = container  || {};
    container.response = require('../helpers/response');

    var customerApp = require('./customer/default.controller')(container);
    var orderApp = require('./order/default.controller')(container);
    var asyncApp = require('./order/async.controller')(container);
    var serviceApp = require('./service/default.controller')(container);
    var reviewApp = require('./review/default.controller')(container);
    var pricingTechApp = require('./pricingTechnique/default.controller')(container);
    var contractorApp = require('./contractor/default.controller')(container);
    var dashboardApp = require('./dashboard/default.controller')(container);


    var app = new express();
    var routes = new express.Router();

    routes.use(['/customer', '/customers'], customerApp);
    routes.use([''], dashboardApp);
    routes.use(['/order', '/orders'], orderApp);
    routes.use(['/service', '/services'], serviceApp);
    routes.use(['/review', '/reviews'], reviewApp);
    routes.use(['/pricingTechnique', '/pricingTechnique'], pricingTechApp );
    routes.use(['/contractor', '/contractors'], contractorApp);
    routes.use(['/async'], asyncApp);

    /**
     *
     */
    routes.get('/', function(req, res){
        var data = {code: 200, 'data' : [
            'chocolate', 'Egg', 'Milk', 'Chips', 'Mario',
            'Potato', 'Empty Product'
        ]};
        res.send(data);
    });

    app.use('/v1', routes);
    return routes;
};