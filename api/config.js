"use strict";

const config = {
  mongodb : "mongodb://localhost/welldone",
  base_url: "localhost:8080"
};

module.exports = config;