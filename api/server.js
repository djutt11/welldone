'use strict';

const express = require('express');
const mongoose = require('mongoose');
var path = require('path');
var cons = require('consolidate');
const config = require('./config');
const bodyParser = require('body-parser');
const winston = require('winston');
var cors = require('cors');
var logger = new (winston.Logger)({
    level: 'info',
    transports: [
        new (winston.transports.Console)(),
        new(winston.transports.File)({filename: './runtime/logs/app.log'})
    ]
});

var container = {};
container.logger = logger;
const routes = require('./modules/routes')(container);

// Constants
const PORT = process.env.server_port || 8080;
const HOST = process.env.server_host || "0.0.0.0";
const app = express();
const dbURL = process.env.mongo_host || "mongodb://localhost/welldone";

logger.info( "***********************",dbURL);

app.engine('html',cons.swig);
app.set('views',path.join(__dirname,'views'));
app.set('view engine','html');

app.use(express.static(path.join(__dirname,'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use("", routes);
app.use(config);


const db = mongoose.connect(dbURL, { useMongoClient: true });
mongoose.Promise = global.Promise;

db.on('error', function(){
    logger.error(arguments);
});
db.once('open', function() {
    app.listen(PORT, HOST);
    logger.info('Running on http://'+HOST+':'+PORT);

});

