'use strict';
var error = function(code, message){
    return {code, message};
};

var success = function(data){
    return {
        'code':200, 'data' : data
    };
}

module.exports = {
    error, success
};
