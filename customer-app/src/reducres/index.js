import {NEW_ORDER, SET_LIST} from '../constants';

const orderList = (list = [],action) => {

    switch (action.type) {
        case SET_LIST:

            console.log('action'.action);

            return action.list;
        case NEW_ORDER:
            list.unshift(action.order);
            return list.slice();
        default:
            return list;
    }
};

export default orderList;