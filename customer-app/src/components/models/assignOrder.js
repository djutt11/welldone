import React, { Component } from 'react';


import {Button, Modal} from 'react-bootstrap'

class AssignOrder extends Component {


constructor(props){
    super(props);

    this.state={
        showModal:true
    }
}


    close() {
        this.setState({ showModal: false });
    }
    open() {
        this.setState({ showModal: true });
    }

    render() {

    console.log('child props',this.props);
    const me=this;
        return (
            <div >
                <Modal show={this.state.showModal} onHide={me.close}>
                    <Modal.Header closeButton>
                        <Modal.Title>Modal heading</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h4>Text in a modal</h4>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={me.close}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default AssignOrder;
