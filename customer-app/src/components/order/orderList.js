import React, { Component } from 'react';
import {connect} from 'react-redux';
import {ALL_ORDERS} from '../../api/apiUrls';
import {setOrderList} from '../../actions';

import AssignOrder from '../models/assignOrder'


import Pagination from "react-js-pagination";


class OrderList extends Component {

    constructor(props){
       super(props);
        this.state = {
            activePage: 15,
            isOpen: false,
            orderId : null
        };
    }

    componentDidMount(){

        const me=this;


        fetch(ALL_ORDERS, {
            method:'GET',
            headers: {
                Accept: 'application/json',
            }
            })
            .then(response => {
                return response.json();
            }).then(data=>{


                console.log('data.data',data.data);

            me.props.setOrderList(data.data.docs);

            // console.log('api data state ',this.state.orders);

        })


    }
    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        this.setState({activePage: pageNumber});
    }



    toggleModel(orderId){

        console.log('pop called ');
        this.setState({isOpen: true,orderId:id});
    }
    render() {

        console.log('this.props',this.props);
        const me=this;
        return (
            <div>

                <h1>order List list</h1>

                {me.state.isOpen && <AssignOrder props={me.state}></AssignOrder>}

{/*
                <div>
                    <Pagination
                        activePage={this.state.activePage}
                        itemsCountPerPage={10}
                        totalItemsCount={450}
                        pageRangeDisplayed={5}
                        onChange={()=>me.handlePageChange(event.target.value)}
                    />
                </div>
*/}

                <ul>
                    {

                        this.props.order.map(function (order) {

                            return<div>
                                <a href="javascript:void(0)" onClick={()=>me.toggleModel()}><li>
                                    {order.notes}
                                    </li></a>
                            </div>
                        })
                    }

                </ul>


            </div>
        );
    }
}

const mapStatToProps = (order) => {
    return {order};
};

const mapDispatchToProps = () => {
    return {
        setOrderList
    }
};

export default connect(mapStatToProps,mapDispatchToProps())(OrderList);
