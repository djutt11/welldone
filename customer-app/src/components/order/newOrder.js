import React, { Component } from 'react';

import {connect} from 'react-redux';

import {FormControl , FormGroup,Button,ControlLabel} from 'react-bootstrap'
 import {ALL_CUSTOMER,ALL_SERVICES,CREATE_ORDER,VIEW_ORDER} from '../../api/apiUrls';
import {setOrderList,createOrder} from "../../actions";

class NewOrder extends Component {

    constructor(props){
        super(props)
        this.state={
            notes:'',
            services: [],
            customers: [],
            service:'',
            customer:''
        }
    }

    componentDidMount(){

        const me=this;

        /* load customers*/
        fetch(ALL_CUSTOMER, {
            method:'GET',
            headers: {Accept: 'application/json',}
        })
        .then(response => {
            return response.json();
        }).then(data=>{
        me.setState({customers:data.data})
        console.log('customers',this.state.customers);
        })

        /*load services*/
        fetch(ALL_SERVICES, {
            method:'GET',
            headers: {Accept: 'application/json',}
        })
        .then(response => {
            return response.json();
        }).then(data=>{
        me.setState({services:data.data})
        console.log('services ',this.state.services);
        })


    }

    createOrder(){

        const me=this;
        let postService;
        let postCustomer;

        this.state.customers.map(function (customer) {
            if(customer.firstName === me.state.customer){
                postCustomer=customer._id;
                return;
            }
        })
        this.state.services.map(function (service) {
            if(service.name === me.state.service){
                postService=service._id;
                return;
            }
        })

        fetch(CREATE_ORDER, {
            method:'POST',
            headers: {'content-type': 'application/json'},
            body: JSON.stringify({
                customer: postCustomer,
                service: postService,
                notes: me.state.notes
            })
        })
            .then(response => {
                return response.json();
            }).then(data=>{
                console.log('success create order',data)
            me.props.createOrder(data.data);

        })


    }

    render() {
        return (
            <div>
                <h1>create new order</h1>
                <form >

                    <input
                        className="form-control"
                        placeholder="Order Notes"
                        value={this.state.notes}
                        onChange={event=> this.setState({notes: event.target.value})}
                    />

                    <FormGroup controlId="formControlsSelect">
                        <ControlLabel>Select Customer</ControlLabel>
                        <FormControl componentClass="select" placeholder="select" value={this.state.customer} onChange={event=> this.setState({customer: event.target.value})}>
                            {
                                this.state.customers.map(function (customer) {
                                    return <option value={customer.firstName} >{customer.firstName}</option>
                                })
                            }

                        </FormControl>
                    </FormGroup>

                    <FormGroup controlId="formControlsSelect">
                        <ControlLabel>Select Service</ControlLabel>
                        <FormControl componentClass="select" placeholder="select" value={this.state.service} onChange={event=> this.setState({service: event.target.value})}>
                            {
                                this.state.services.map(function (service) {
                                    return <option value={service.name} >{service.name}</option>
                                })
                            }

                        </FormControl>
                    </FormGroup>

                    <Button  onClick={()=>this.createOrder()}>
                        Create Order
                    </Button>
                </form>
            </div>
        );
    }
}


const mapStatToProps = (order) => {
    return {order};
};



const mapDispatchToProps = () => {
    return {
        createOrder
    }
};


export default connect(null,mapDispatchToProps())(NewOrder);
