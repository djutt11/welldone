import React, { Component } from 'react';
import logo from '../../logo.svg';
import '../../App.css';



class ContractorApp extends Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 className="App-title">Welcome to Contractor App</h1>
                </header>
                <p className="App-intro">
                    All Activities related to contractors
                </p>

                <div className="row">
                    <div className="col-sm-8">
                       List of contractor
                    </div>
                    <div className="col-sm-4">
                        order assigned to contractor
                    </div>
                </div>



            </div>
        );
    }
}

export default ContractorApp;
