export const API_SERVER = 'http://0.0.0.0:8080/';
export const ALL_CUSTOMER = API_SERVER + 'customer';
export const ALL_SERVICES = API_SERVER + 'services';
export const ALL_ORDERS = API_SERVER + 'order';
export const CREATE_ORDER = API_SERVER + 'order';
export const VIEW_ORDER='http://0.0.0.0:8080/order/:id';