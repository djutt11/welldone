/*
*  Action Constants
* */
export const SET_LIST = 'SET_LIST';
export const NEW_ORDER = 'NEW_ORDER';

/*
*  Routes Constants
* */

export const CUSTOMER_ROUTE = 'customer';
export const CONTRACTOR_ROUTE = 'contractor';
