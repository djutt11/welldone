import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import ContractorApp from './components/contractor/contractorApp'
import registerServiceWorker from './registerServiceWorker';

import { Router, Route, hashHistory} from 'react-router';


import {Provider} from 'react-redux'
import {createStore} from 'redux'

import orderList from './reducres';

import {CUSTOMER_ROUTE,CONTRACTOR_ROUTE} from './constants';


const store = createStore(orderList);

ReactDOM.render(
    <Provider store={store}>
        <Router path="/" history={hashHistory}>
            <Route path={CUSTOMER_ROUTE} component={App}/>
            <Route path={CONTRACTOR_ROUTE} component={ContractorApp}/>
        </Router>

    </Provider>
 ,

    document.getElementById('root'));
registerServiceWorker();
