import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import OrderList from './components/order/orderList'
import NewOrder from './components/order/newOrder'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>

          <div className="row">
              <div className="col-sm-8">
                  <OrderList></OrderList>
              </div>
              <div className="col-sm-4">
                  <NewOrder></NewOrder>
              </div>
          </div>



      </div>
    );
  }
}

export default App;
