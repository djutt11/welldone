import {SET_LIST,NEW_ORDER} from '../constants';


export const setOrderList = (list) => {

    return {
        type:SET_LIST,
        list
    }
};

export const createOrder = (order)=>{
    return{
        type : NEW_ORDER,
        order
    }
}