-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: sakrobe_db
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.17.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

use tracker_db;

--
-- Table structure for table `app_option`
--

DROP TABLE IF EXISTS `app_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_option` (
  `opt_name` varchar(45) NOT NULL,
  `opt_value` varchar(255) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`opt_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_option`
--

LOCK TABLES `app_option` WRITE;
/*!40000 ALTER TABLE `app_option` DISABLE KEYS */;
INSERT INTO `app_option` (`opt_name`, `opt_value`, `data`) VALUES ('app_sp_commission','10','Default Sakrobe Commission');
/*!40000 ALTER TABLE `app_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_session`
--

DROP TABLE IF EXISTS `app_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_session` (
  `id` char(40) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob,
  PRIMARY KEY (`id`),
  KEY `idx_session_expire` (`expire`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_session`
--

LOCK TABLES `app_session` WRITE;
/*!40000 ALTER TABLE `app_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `fk_auth_assignment_user1_idx` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_auth_assignment_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('access-to-customer-section',2,'Allow you to see Customer List','CustomerAccessRule',NULL,1511256652,1511256652),('can-view-bill',2,'Allow you to see Sakrobe Bills.','BillAccessRule','a:1:{s:4:\"repo\";s:4:\"bill\";}',1511256652,1511256652),('can-view-invoice',2,'Allow you to see details of an Invoice.','InvoiceAccessRule','a:1:{s:4:\"repo\";s:7:\"invoice\";}',1511256652,1511256652),('cancel-invoice',2,'Allow you to mark any invoice as cancel.','InvoiceAccessRule',NULL,1511256652,1511256652),('cancel-order',2,'Can cancel any job','OrderAccessRule',NULL,1511256651,1511256651),('create-expert',2,'Can create new service providers/experts',NULL,NULL,1511256652,1511256652),('delete-order',2,'Can delete any job','OrderAccessRule',NULL,1511256651,1511256651),('disable-customer',2,'Allow you to disbale/enable customer','CustomerAccessRule',NULL,1511256652,1511256652),('disable-expert',2,'Allow to disable/enable service provider accounts',NULL,NULL,1511256652,1511256652),('edit-customer',2,'Allow you to edit customer details','CustomerAccessRule',NULL,1511256652,1511256652),('edit-expert',2,'Allow to edit properties of a service provider/expert',NULL,NULL,1511256652,1511256652),('edit-invoice',2,'Allow you to edit an invoice','InvoiceAccessRule',NULL,1511256652,1511256652),('edit-order',2,'Can edit any job','OrderAccessRule',NULL,1511256651,1511256651),('generate-invoice',2,'Allow you to generate a new invoice.','InvoiceAccessRule',NULL,1511256652,1511256652),('my-dashboard',2,'Permission to view Dashboard',NULL,NULL,1511256651,1511256651),('my-invoices',2,'Allow you to see Own Invoices Only','InvoiceAccessRule',NULL,1511256652,1511256652),('new-order',2,'Can Post new Order','OrderAccessRule',NULL,1511256652,1511256652),('p_my_profile',2,'Permission to view My Profile',NULL,NULL,1511256651,1511256651),('p-change-password',2,'Permission to Change Password',NULL,NULL,1511256651,1511256651),('role_abstract',1,'Basic Role with Generic Permissions',NULL,NULL,1511256652,1511256652),('role_app_manager',1,'App Manager Role, Can only see customers or SP of defined Region',NULL,NULL,1511256654,1511256654),('role_app_master',1,'Master of every thing.. Super User.',NULL,NULL,1511256655,1511256655),('role_customer',1,'Customer Role',NULL,NULL,1511256653,1511256653),('role_invoice_manager',1,'Invoice Generator Role',NULL,NULL,1511256653,1511256653),('role_manage_service_provider',1,'Grouped Role',NULL,NULL,1511256653,1511256653),('role_service_provider',1,'Service Provider Role',NULL,NULL,1511256654,1511256654),('see-customer',2,'Allow you to see customer Details.','CustomerAccessRule',NULL,1511256652,1511256652),('see-invoices',2,'Allow you to see system generated Invoices','InvoiceAccessRule',NULL,1511256652,1511256652),('sys_config_role',1,'Role to manage system configurations.',NULL,NULL,1511256655,1511256655),('view-expert',2,'Allow to see any service providers/experts',NULL,NULL,1511256652,1511256652),('view-order',2,'Can view orders','OrderAccessRule',NULL,1511256651,1511256651);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` (`parent`, `child`) VALUES ('role_app_manager','access-to-customer-section'),('role_app_master','access-to-customer-section'),('role_app_manager','can-view-bill'),('role_app_master','can-view-bill'),('role_service_provider','can-view-bill'),('role_invoice_manager','can-view-invoice'),('role_invoice_manager','cancel-invoice'),('role_customer','cancel-order'),('role_service_provider','cancel-order'),('role_manage_service_provider','create-expert'),('role_customer','delete-order'),('role_service_provider','delete-order'),('role_app_master','disable-customer'),('role_manage_service_provider','disable-expert'),('role_app_master','edit-customer'),('role_manage_service_provider','edit-expert'),('role_invoice_manager','edit-invoice'),('role_customer','edit-order'),('role_service_provider','edit-order'),('role_invoice_manager','generate-invoice'),('role_abstract','my-dashboard'),('role_customer','new-order'),('role_abstract','p_my_profile'),('role_abstract','p-change-password'),('role_app_manager','role_abstract'),('role_customer','role_abstract'),('role_service_provider','role_abstract'),('sys_config_role','role_abstract'),('role_app_master','role_app_manager'),('role_app_manager','role_invoice_manager'),('role_app_master','role_invoice_manager'),('role_customer','role_invoice_manager'),('role_service_provider','role_invoice_manager'),('role_app_manager','role_manage_service_provider'),('role_app_master','role_manage_service_provider'),('role_app_manager','see-customer'),('role_app_master','see-customer'),('role_invoice_manager','see-invoices'),('role_app_master','sys_config_role'),('role_manage_service_provider','view-expert'),('role_app_manager','view-order'),('role_app_master','view-order'),('role_customer','view-order'),('role_service_provider','view-order');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES ('BillAccessRule','O:26:\"common\\rbac\\BillAccessRule\":3:{s:4:\"name\";s:14:\"BillAccessRule\";s:9:\"createdAt\";i:1511256651;s:9:\"updatedAt\";i:1511256651;}',1511256651,1511256651),('CustomerAccessRule','O:30:\"common\\rbac\\CustomerAccessRule\":3:{s:4:\"name\";s:18:\"CustomerAccessRule\";s:9:\"createdAt\";i:1511256650;s:9:\"updatedAt\";i:1511256650;}',1511256650,1511256650),('InvoiceAccessRule','O:29:\"common\\rbac\\InvoiceAccessRule\":4:{s:4:\"name\";s:17:\"InvoiceAccessRule\";s:42:\"\0common\\rbac\\InvoiceAccessRule\0invoiceRepo\";N;s:9:\"createdAt\";i:1511256650;s:9:\"updatedAt\";i:1511256650;}',1511256650,1511256650),('isAuthor','O:20:\"lime\\rbac\\AuthorRule\":3:{s:4:\"name\";s:8:\"isAuthor\";s:9:\"createdAt\";i:1511256648;s:9:\"updatedAt\";i:1511256648;}',1511256648,1511256648),('OrderAccessRule','O:27:\"common\\rbac\\OrderAccessRule\":3:{s:4:\"name\";s:15:\"OrderAccessRule\";s:9:\"createdAt\";i:1511256650;s:9:\"updatedAt\";i:1511256650;}',1511256650,1511256650),('RecordAccessRule','O:28:\"common\\rbac\\RecordAccessRule\":3:{s:4:\"name\";s:16:\"RecordAccessRule\";s:9:\"createdAt\";i:1511256650;s:9:\"updatedAt\";i:1511256650;}',1511256650,1511256650);
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_token`
--

DROP TABLE IF EXISTS `auth_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_token` (
  `token_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `token_type_id` int(10) unsigned NOT NULL,
  `token` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`token_id`),
  KEY `fk_token_user1_idx` (`user_id`),
  KEY `fk_token_record_type1_idx` (`token_type_id`),
  CONSTRAINT `fk_token_record_type1` FOREIGN KEY (`token_type_id`) REFERENCES `record_type` (`record_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_token_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_token`
--

LOCK TABLES `auth_token` WRITE;
/*!40000 ALTER TABLE `auth_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bid`
--

DROP TABLE IF EXISTS `bid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bid` (
  `bid_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) unsigned NOT NULL,
  `expert_id` bigint(20) unsigned NOT NULL,
  `status_id` int(10) unsigned NOT NULL,
  `amount` varchar(45) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `notes` text,
  `schedule_on` datetime DEFAULT NULL,
  `schedule_notes` text,
  PRIMARY KEY (`bid_id`),
  KEY `fk_bid_job1_idx` (`order_id`),
  KEY `fk_bid_expert1_idx` (`expert_id`),
  KEY `fk_bid_status1_idx` (`status_id`),
  CONSTRAINT `fk_bid_expert1` FOREIGN KEY (`expert_id`) REFERENCES `expert` (`expert_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_bid_job1` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_bid_status1` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bid`
--

LOCK TABLES `bid` WRITE;
/*!40000 ALTER TABLE `bid` DISABLE KEYS */;
/*!40000 ALTER TABLE `bid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill`
--

DROP TABLE IF EXISTS `bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill` (
  `bill_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `status_id` int(10) unsigned NOT NULL,
  `expert_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `notes` text,
  `updated_at` timestamp NULL DEFAULT NULL,
  `payment_status` int(10) unsigned NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `amount` decimal(10,3) unsigned DEFAULT NULL,
  PRIMARY KEY (`bill_id`),
  KEY `fk_bill_status1_idx` (`status_id`),
  KEY `fk_bill_expert1_idx` (`expert_id`),
  KEY `fk_bill_status2_idx` (`payment_status`),
  CONSTRAINT `fk_bill_expert1` FOREIGN KEY (`expert_id`) REFERENCES `expert` (`expert_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_bill_status1` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_bill_status2` FOREIGN KEY (`payment_status`) REFERENCES `status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill`
--

LOCK TABLES `bill` WRITE;
/*!40000 ALTER TABLE `bill` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupon`
--

DROP TABLE IF EXISTS `coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon` (
  `coupon_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `notes` text,
  `value` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`coupon_id`),
  KEY `fk_coupon_status1_idx` (`status_id`),
  CONSTRAINT `fk_coupon_status1` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon`
--

LOCK TABLES `coupon` WRITE;
/*!40000 ALTER TABLE `coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupon_service`
--

DROP TABLE IF EXISTS `coupon_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon_service` (
  `coupon_service_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` bigint(20) unsigned NOT NULL,
  `service_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`coupon_service_id`),
  KEY `fk_coupon_service_coupon1_idx` (`coupon_id`),
  KEY `fk_coupon_service_record1_idx` (`service_id`),
  CONSTRAINT `fk_coupon_service_coupon1` FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`coupon_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_coupon_service_record1` FOREIGN KEY (`service_id`) REFERENCES `record` (`record_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon_service`
--

LOCK TABLES `coupon_service` WRITE;
/*!40000 ALTER TABLE `coupon_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device`
--

DROP TABLE IF EXISTS `device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device` (
  `device_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`device_id`),
  KEY `fk_device_user1_idx` (`user_id`),
  CONSTRAINT `fk_device_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device`
--

LOCK TABLES `device` WRITE;
/*!40000 ALTER TABLE `device` DISABLE KEYS */;
/*!40000 ALTER TABLE `device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_template`
--

DROP TABLE IF EXISTS `email_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_template` (
  `email_template_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `template_type_id` bigint(20) unsigned NOT NULL,
  `from` varchar(255) DEFAULT NULL,
  `reply_to` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `html` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email_template_id`),
  KEY `fk_email_template_email_template_type1_idx` (`template_type_id`),
  CONSTRAINT `fk_email_template_email_template_type1` FOREIGN KEY (`template_type_id`) REFERENCES `email_template_type` (`email_template_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_template`
--

LOCK TABLES `email_template` WRITE;
/*!40000 ALTER TABLE `email_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_template_type`
--

DROP TABLE IF EXISTS `email_template_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_template_type` (
  `email_template_type_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`email_template_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_template_type`
--

LOCK TABLES `email_template_type` WRITE;
/*!40000 ALTER TABLE `email_template_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_template_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expert`
--

DROP TABLE IF EXISTS `expert`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expert` (
  `expert_id` bigint(20) unsigned NOT NULL,
  `commission` int(11) DEFAULT NULL,
  `available` int(1) unsigned DEFAULT '0',
  `wh_from` time DEFAULT NULL,
  `wh_to` time DEFAULT NULL,
  PRIMARY KEY (`expert_id`),
  KEY `fk_expert_user1_idx` (`expert_id`),
  CONSTRAINT `fk_expert_user1` FOREIGN KEY (`expert_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expert`
--

LOCK TABLES `expert` WRITE;
/*!40000 ALTER TABLE `expert` DISABLE KEYS */;
/*!40000 ALTER TABLE `expert` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expert_service`
--

DROP TABLE IF EXISTS `expert_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expert_service` (
  `expert_service_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `expert_id` bigint(20) unsigned NOT NULL,
  `service_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`expert_service_id`),
  KEY `fk_expert_service_expert1_idx` (`expert_id`),
  KEY `fk_expert_service_record1_idx` (`service_id`),
  CONSTRAINT `fk_expert_service_expert1` FOREIGN KEY (`expert_id`) REFERENCES `expert` (`expert_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_expert_service_record1` FOREIGN KEY (`service_id`) REFERENCES `record` (`record_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expert_service`
--

LOCK TABLES `expert_service` WRITE;
/*!40000 ALTER TABLE `expert_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `expert_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image` (
  `image_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `record_type_id` int(10) unsigned NOT NULL,
  `item_id` bigint(20) unsigned NOT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `uploaded_path` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`image_id`),
  KEY `fk_image_record_type1_idx` (`record_type_id`),
  CONSTRAINT `fk_image_record_type1` FOREIGN KEY (`record_type_id`) REFERENCES `record_type` (`record_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inv_line`
--

DROP TABLE IF EXISTS `inv_line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_line` (
  `inv_line_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` bigint(20) NOT NULL,
  `price` decimal(10,3) DEFAULT NULL,
  `notes` text,
  `code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`inv_line_id`),
  KEY `fk_inv_line_invoice1_idx` (`invoice_id`),
  CONSTRAINT `fk_inv_line_invoice1` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`invoice_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_line`
--

LOCK TABLES `inv_line` WRITE;
/*!40000 ALTER TABLE `inv_line` DISABLE KEYS */;
/*!40000 ALTER TABLE `inv_line` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `invoice_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inv_no` varchar(45) DEFAULT NULL,
  `bid_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `amount` decimal(10,3) DEFAULT NULL,
  `status_id` int(10) unsigned NOT NULL,
  `payment_status` int(10) unsigned NOT NULL,
  `commission` int(11) DEFAULT NULL,
  `commission_amount` decimal(10,3) DEFAULT NULL,
  `coupon_code` varchar(45) DEFAULT NULL,
  `discount_amount` decimal(10,3) DEFAULT NULL,
  `notes` text,
  `bill_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`invoice_id`),
  KEY `fk_invoice_bid1_idx` (`bid_id`),
  KEY `fk_invoice_status1_idx` (`status_id`),
  KEY `fk_invoice_status2_idx` (`payment_status`),
  KEY `fk_invoice_bill1_idx` (`bill_id`),
  CONSTRAINT `fk_invoice_bid1` FOREIGN KEY (`bid_id`) REFERENCES `bid` (`bid_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_invoice_bill1` FOREIGN KEY (`bill_id`) REFERENCES `bill` (`bill_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_invoice_status1` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_invoice_status2` FOREIGN KEY (`payment_status`) REFERENCES `status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `map_shape`
--

DROP TABLE IF EXISTS `map_shape`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `map_shape` (
  `map_shape_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `expert_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`map_shape_id`),
  KEY `fk_map_shape_expert1_idx` (`expert_id`),
  CONSTRAINT `fk_map_shape_expert1` FOREIGN KEY (`expert_id`) REFERENCES `expert` (`expert_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `map_shape`
--

LOCK TABLES `map_shape` WRITE;
/*!40000 ALTER TABLE `map_shape` DISABLE KEYS */;
/*!40000 ALTER TABLE `map_shape` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `order_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` bigint(20) unsigned NOT NULL,
  `customer_id` bigint(20) unsigned NOT NULL,
  `city_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `notes` text,
  `promo_code` varchar(45) DEFAULT NULL,
  `map_lat` decimal(10,8) DEFAULT NULL,
  `map_lng` decimal(11,8) DEFAULT NULL,
  `location` varchar(500) DEFAULT NULL,
  `status_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `added_on` datetime DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `fk_job_record1_idx` (`service_id`),
  KEY `fk_job_status1_idx` (`status_id`),
  KEY `fk_order_user1_idx` (`customer_id`),
  KEY `idx_order_city` (`city_id`),
  CONSTRAINT `fk_job_record1` FOREIGN KEY (`service_id`) REFERENCES `record` (`record_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_status1` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_city` FOREIGN KEY (`city_id`) REFERENCES `record` (`record_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_user1` FOREIGN KEY (`customer_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_history`
--

DROP TABLE IF EXISTS `order_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_history` (
  `order_history_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status_id` int(10) unsigned NOT NULL,
  `notes` text,
  `data` text,
  PRIMARY KEY (`order_history_id`),
  KEY `fk_order_history_order1_idx` (`order_id`),
  KEY `fk_order_history_status1_idx` (`status_id`),
  CONSTRAINT `fk_order_history_order1` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_history_status1` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_history`
--

LOCK TABLES `order_history` WRITE;
/*!40000 ALTER TABLE `order_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outgoing_email`
--

DROP TABLE IF EXISTS `outgoing_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outgoing_email` (
  `outgoing_email_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email_to` varchar(255) DEFAULT NULL,
  `email_from` varchar(255) DEFAULT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `reply_to` varchar(45) DEFAULT NULL,
  `content` text,
  `data` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `send_at` timestamp NULL DEFAULT NULL,
  `is_send` bit(1) DEFAULT NULL,
  PRIMARY KEY (`outgoing_email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outgoing_email`
--

LOCK TABLES `outgoing_email` WRITE;
/*!40000 ALTER TABLE `outgoing_email` DISABLE KEYS */;
/*!40000 ALTER TABLE `outgoing_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quick_filter`
--

DROP TABLE IF EXISTS `quick_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quick_filter` (
  `filter_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `record_type_id` int(10) unsigned NOT NULL,
  `data` longtext,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`filter_id`),
  KEY `fk_quick_filter_user1_idx` (`user_id`),
  KEY `fk_quick_filter_record_type1_idx` (`record_type_id`),
  CONSTRAINT `fk_quick_filter_record_type1` FOREIGN KEY (`record_type_id`) REFERENCES `record_type` (`record_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_quick_filter_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quick_filter`
--

LOCK TABLES `quick_filter` WRITE;
/*!40000 ALTER TABLE `quick_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `quick_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `record`
--

DROP TABLE IF EXISTS `record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `record` (
  `record_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `name_ar` varchar(255) DEFAULT NULL,
  `notes` text,
  `record_type_id` int(10) unsigned NOT NULL,
  `parent_id` bigint(20) unsigned DEFAULT NULL,
  `status_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`record_id`),
  KEY `fk_record_record_type1_idx` (`record_type_id`),
  KEY `fk_record_record1_idx` (`parent_id`),
  KEY `fk_record_status1_idx` (`status_id`),
  CONSTRAINT `fk_record_record1` FOREIGN KEY (`parent_id`) REFERENCES `record` (`record_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_record_record_type1` FOREIGN KEY (`record_type_id`) REFERENCES `record_type` (`record_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_record_status1` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `record`
--

LOCK TABLES `record` WRITE;
/*!40000 ALTER TABLE `record` DISABLE KEYS */;
/*!40000 ALTER TABLE `record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `record_type`
--

DROP TABLE IF EXISTS `record_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `record_type` (
  `record_type_id` int(10) unsigned NOT NULL,
  `parent` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`record_type_id`),
  KEY `fk_record_type_record_type1_idx` (`parent`),
  CONSTRAINT `fk_record_type_record_type1` FOREIGN KEY (`parent`) REFERENCES `record_type` (`record_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `record_type`
--

LOCK TABLES `record_type` WRITE;
/*!40000 ALTER TABLE `record_type` DISABLE KEYS */;
INSERT INTO `record_type` (`record_type_id`, `parent`, `name`, `created_at`, `updated_at`) VALUES (1,NULL,'USER','2017-11-21 14:30:46','2017-11-21 14:30:46'),(11,NULL,'Service','2017-11-21 14:30:46','2017-11-21 14:30:46'),(12,NULL,'Orders','2017-11-21 14:30:46','2017-11-21 14:30:46'),(13,NULL,'Invoices','2017-11-21 14:30:46','2017-11-21 14:30:46'),(14,NULL,'Sakorbe Bill','2017-11-21 14:30:46','2017-11-21 14:30:46'),(20,NULL,'Location: Country','2017-11-21 14:30:46','2017-11-21 14:30:46'),(21,20,'Location: City','2017-11-21 14:30:46','2017-11-21 14:30:46'),(22,21,'Location: CityArea','2017-11-21 14:30:46','2017-11-21 14:30:46'),(23,22,'Location: Block','2017-11-21 14:30:46','2017-11-21 14:30:46'),(31,NULL,'Property Type','2017-11-21 14:30:46','2017-11-21 14:30:46'),(50,NULL,'Contact Phones','2017-11-21 14:30:46','2017-11-21 14:30:46'),(51,50,'Home Primary','2017-11-21 14:30:46','2017-11-21 14:30:46'),(52,50,'Home Secondary','2017-11-21 14:30:46','2017-11-21 14:30:46'),(53,50,'Work Primary','2017-11-21 14:30:46','2017-11-21 14:30:46'),(54,50,'Work Secondary','2017-11-21 14:30:46','2017-11-21 14:30:46'),(55,50,'Skype','2017-11-21 14:30:46','2017-11-21 14:30:46'),(100,NULL,'Forgot Password','2017-11-21 14:30:46','2017-11-21 14:30:46'),(101,NULL,'API Auth Token','2017-11-21 14:30:46','2017-11-21 14:30:46'),(102,NULL,'API-Token: Customer App','2017-11-21 14:30:46','2017-11-21 14:30:46'),(103,NULL,'API-Token: SP App','2017-11-21 14:30:46','2017-11-21 14:30:46');
/*!40000 ALTER TABLE `record_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `review_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) unsigned NOT NULL,
  `customer_id` bigint(20) unsigned NOT NULL,
  `expert_id` bigint(20) unsigned DEFAULT NULL,
  `data` text,
  `rating` int(11) DEFAULT NULL,
  `notes` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`review_id`),
  KEY `fk_review_job1_idx` (`order_id`),
  KEY `fk_review_user1_idx` (`customer_id`),
  KEY `fk_review_expert1_idx` (`expert_id`),
  CONSTRAINT `fk_review_expert1` FOREIGN KEY (`expert_id`) REFERENCES `expert` (`expert_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_review_job1` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_review_user1` FOREIGN KEY (`customer_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shape_point`
--

DROP TABLE IF EXISTS `shape_point`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shape_point` (
  `shape_point_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `map_shape_id` bigint(20) unsigned NOT NULL,
  `lat` decimal(10,8) DEFAULT NULL,
  `lng` decimal(11,8) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`shape_point_id`),
  KEY `fk_shape_point_map_shape1_idx` (`map_shape_id`),
  CONSTRAINT `fk_shape_point_map_shape1` FOREIGN KEY (`map_shape_id`) REFERENCES `map_shape` (`map_shape_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shape_point`
--

LOCK TABLES `shape_point` WRITE;
/*!40000 ALTER TABLE `shape_point` DISABLE KEYS */;
/*!40000 ALTER TABLE `shape_point` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `status_id` int(10) unsigned NOT NULL,
  `record_type_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`status_id`),
  KEY `fk_status_record_type1_idx` (`record_type_id`),
  CONSTRAINT `fk_status_record_type1` FOREIGN KEY (`record_type_id`) REFERENCES `record_type` (`record_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` (`status_id`, `record_type_id`, `name`, `created_at`, `updated_at`) VALUES (1,NULL,'Active','2017-11-21 09:30:47','2017-11-21 09:30:47'),(2,NULL,'Disabled','2017-11-21 09:30:47','2017-11-21 09:30:47'),(3,NULL,'Inprogress','2017-11-21 09:30:47','2017-11-21 09:30:47'),(4,NULL,'Draft','2017-11-21 09:30:47','2017-11-21 09:30:47'),(5,NULL,'Deleted','2017-11-21 09:30:47','2017-11-21 09:30:47'),(6,NULL,'Pending','2017-11-21 09:30:47','2017-11-21 09:30:47'),(7,NULL,'Cancelled','2017-11-21 09:30:47','2017-11-21 09:30:47'),(10,12,'Pending','2017-11-21 09:30:47','2017-11-21 09:30:47'),(11,12,'Work In Progess','2017-11-21 09:30:47','2017-11-21 09:30:47'),(12,12,'Completed','2017-11-21 09:30:47','2017-11-21 09:30:47'),(13,12,'Dispute','2017-11-21 09:30:47','2017-11-21 09:30:47'),(14,12,'Dispute Resolved','2017-11-21 09:30:47','2017-11-21 09:30:47'),(15,12,'Assigned','2017-11-21 09:30:47','2017-11-21 09:30:47'),(16,12,'Requested by Customer','2017-11-21 09:30:47','2017-11-21 09:30:47'),(17,12,'Accepted by Service Provider','2017-11-21 09:30:47','2017-11-21 09:30:47'),(18,12,'Scheduled for Meeting','2017-11-21 09:30:47','2017-11-21 09:30:47'),(20,12,'Order Cancelled','2017-11-21 09:30:47','2017-11-21 09:30:47'),(21,12,'Order Rejected','2017-11-21 09:30:47','2017-11-21 09:30:47'),(22,12,'Under Study','2017-11-21 09:30:47','2017-11-21 09:30:47'),(31,13,'Draft Invoice','2017-11-21 09:30:47','2017-11-21 09:30:47'),(32,13,'Cancelled Invoice','2017-11-21 09:30:47','2017-11-21 09:30:47'),(33,12,'Invoice Generated','2017-11-21 09:30:47','2017-11-21 09:30:47'),(35,13,'Payment Pending','2017-11-21 09:30:47','2017-11-21 09:30:47'),(36,13,'Payment Received','2017-11-21 09:30:47','2017-11-21 09:30:47'),(41,14,'Bill Status:Pending','2017-11-21 09:30:47','2017-11-21 09:30:47'),(42,14,'Bill Status:Cancelled','2017-11-21 09:30:47','2017-11-21 09:30:47'),(43,14,'Bill Status:Generated','2017-11-21 09:30:47','2017-11-21 09:30:47'),(45,14,'Bill Status:Payment Pending','2017-11-21 09:30:47','2017-11-21 09:30:47'),(46,14,'Bill Status:Payment Recevied','2017-11-21 09:30:47','2017-11-21 09:30:47'),(47,14,'Bill Draft','2017-11-21 09:30:47','2017-11-21 09:30:47');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `auth_key` varchar(32) DEFAULT NULL,
  `password_hash` varchar(255) DEFAULT NULL,
  `old_password` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status_id` int(10) unsigned NOT NULL,
  `user_type_id` bigint(20) unsigned NOT NULL,
  `map_lat` decimal(10,8) DEFAULT NULL,
  `map_lng` decimal(11,8) DEFAULT NULL,
  `lang` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_user_status1_idx` (`status_id`),
  KEY `fk_user_user_type1_idx` (`user_type_id`),
  CONSTRAINT `fk_user_status1` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_user_type1` FOREIGN KEY (`user_type_id`) REFERENCES `user_type` (`user_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_address`
--

DROP TABLE IF EXISTS `user_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_address` (
  `user_address_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `city_id` bigint(20) unsigned DEFAULT NULL,
  `map_lat` decimal(10,8) DEFAULT NULL,
  `map_lng` decimal(11,8) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `property_type` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_address_id`),
  KEY `fk_user_address_user1_idx` (`user_id`),
  KEY `fk_user_address_record1_idx` (`city_id`),
  KEY `fk_user_address_record2_idx` (`property_type`),
  CONSTRAINT `fk_user_address_record1` FOREIGN KEY (`city_id`) REFERENCES `record` (`record_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_address_record2` FOREIGN KEY (`property_type`) REFERENCES `record` (`record_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_address_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_address`
--

LOCK TABLES `user_address` WRITE;
/*!40000 ALTER TABLE `user_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_phone`
--

DROP TABLE IF EXISTS `user_phone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_phone` (
  `user_phone_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `phone_type_id` int(10) unsigned NOT NULL,
  `country_code` varchar(10) DEFAULT NULL,
  `p_number` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_phone_id`),
  KEY `fk_user_phone_user1_idx` (`user_id`),
  KEY `fk_user_phone_record_type1_idx` (`phone_type_id`),
  CONSTRAINT `fk_user_phone_record_type1` FOREIGN KEY (`phone_type_id`) REFERENCES `record_type` (`record_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_phone_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_phone`
--

LOCK TABLES `user_phone` WRITE;
/*!40000 ALTER TABLE `user_phone` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_phone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_region`
--

DROP TABLE IF EXISTS `user_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_region` (
  `user_region_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `region_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_region_id`),
  KEY `idx_user_region_users` (`user_id`),
  KEY `idx_user_region_regions` (`region_id`),
  CONSTRAINT `fk_manager_region_record1` FOREIGN KEY (`region_id`) REFERENCES `record` (`record_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_manager_region_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_region`
--

LOCK TABLES `user_region` WRITE;
/*!40000 ALTER TABLE `user_region` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_type`
--

DROP TABLE IF EXISTS `user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_type` (
  `user_type_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `role_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_type`
--

LOCK TABLES `user_type` WRITE;
/*!40000 ALTER TABLE `user_type` DISABLE KEYS */;
INSERT INTO `user_type` (`user_type_id`, `name`, `role_name`) VALUES (1,'Master Admin','role_master_admin'),(2,'App Manager','role_app_manager'),(3,'Service Provider Epert','role_service_provider'),(4,'Customer','role_customer');
/*!40000 ALTER TABLE `user_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-21 14:31:50
