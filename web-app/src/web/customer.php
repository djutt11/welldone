<?php
/**
 * Created by PhpStorm.
 * User: fahad
 * Date: 11/27/17
 * Time: 3:39 PM
 */

use yii\httpclient\Client;
use Yii;

require '../vendor/autoload.php';

$faker = Faker\Factory::create();

$customerURL = 'http://api-instance:8080/v1/customer';

$httpClient = new Client(['baseUrl' => $customerURL]);
$response = $httpClient->createRequest()
    ->setMethod('post')
    ->setData(['name' => 'John Doe', 'email' => 'johndoe@example.com'])
    ->send();

echo "Response ::: <hr /><pre>";
print_r($response);