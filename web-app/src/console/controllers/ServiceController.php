<?php
/**
 * Created by PhpStorm.
 * User: fahad
 * Date: 11/30/17
 * Time: 5:36 PM
 */

namespace console\controllers;


use console\helpers\Fetcher;
use Faker\Factory;
use yii\console\Controller;
use yii\helpers\VarDumper;
use yii\httpclient\Client;

class ServiceController extends BaseConsoleController
{
    /**
     * @return \yii\httpclient\Request
     */
    public function getRequest()
    {
        $request = parent::getRequest();
        $request->setUrl('service');
        return $request;
    }

    /**
     *
     */
    public function actionIndex(){
        $request = $this->getRequest();
        $request->setMethod('get');
        $response = $request->send();
        VarDumper::dump($response->data);
    }


    public function actionCreate(){
        $names = ['Mantainance', 'Plumbering',
            'Electrician', 'Cleaner', 'Jenitor',
            'Air Conditoiner Cooling'];

        foreach ($names as $name){
            $request = $this->getRequest();
            $request->setMethod('post');
            $request->setData(['name' => $name]);
            $response = $request->send();
            VarDumper::dump($response->data);
        }

    }

    public function actionContractors(){


        $request = parent::getRequest();

        $faker = Factory::create();
        $services = Fetcher::services();
        $service = $faker->randomElement($services, 1);

        $request->setUrl('service/'.$service.'/contractors');
        $request->setMethod('get');
        $request->setFormat(Client::FORMAT_URLENCODED);
        $response = $request->send();


        VarDumper::dump($response->data);die;


    }

}