<?php
/**
 * Created by PhpStorm.
 * User: damjad
 * Date: 12/5/17
 * Time: 3:58 PM
 */

namespace console\controllers;


use console\helpers\Fetcher;
use console\helpers\GeoLocation;
use Faker\Factory;
use yii\helpers\Console;
use yii\helpers\VarDumper;
use yii\httpclient\Client;

class ContractorController extends BaseConsoleController
{

    /**
     * @return \yii\httpclient\Request
     */
    public function getRequest()
    {
        $request = parent::getRequest();
        $request->setUrl('/contractor/');
        return $request;
    }

    public function actionIndex(){

        $request = $this->getRequest();
        $filter = [];
        $request->setFormat(Client::FORMAT_URLENCODED);
        $response = $this->getResponse($request, $filter);

        VarDumper::dump($response->data);
    }

    public function actionCreate(){
        $faker = Factory::create();
        $services = Fetcher::services();
        $randomServices = $faker->randomElements($services, 3);
        $contractor = [];
        $contractor['service'] = $randomServices;
        $contractor['name'] = $faker->name;
        $contractor['phone'] = $faker->phoneNumber;
        $request = $this->getRequest();
        $response = $this->postRequest($request, $contractor);
        VarDumper::dump($response->data);

        $contractor = $response->data['data']['_id'];

        $this->addCoverage($contractor, GeoLocation::modeltown());
    }


    /**
     *
     */
    public function actionCoverage(){
        $faker = Factory::create();
        $contractors = Fetcher::contractors();
        $contractor = $contractors[0];
        $contractor = $faker->randomElement($contractors);
        $contractId = $contractor['_id'];
        $this->addCoverage($contractId, GeoLocation::modeltown());
    }


    private function addCoverage($contractId, $coords){

        $this->stdout('ContractID :: ' . $contractId . PHP_EOL);
        $params = ['id' => $contractId, 'coords' => $coords];
        $request = $this->getRequest();
        $request->setUrl("contractor/$contractId/coverage");
        $response = $this->postRequest($request, $params);

        VarDumper::dump($response->data);
    }

}