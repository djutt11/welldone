<?php
/**
 * Created by PhpStorm.
 * User: fahad
 * Date: 11/30/17
 * Time: 5:36 PM
 */

namespace console\controllers;


use yii\console\Controller;
use yii\httpclient\Client;
use yii\httpclient\Request;
use yii\httpclient\Response;

abstract class BaseConsoleController extends Controller
{
    const API_ENDPOINT = 'http://localhost:8080';

    /**
     * @return Client
     */
    public function getClient(){
        $client = new Client(['baseUrl' => self::API_ENDPOINT]);
        return $client;
    }

    /**
     * @return \yii\httpclient\Request
     */
    public function getRequest(){
        $request = $this->getClient()->createRequest();
        $request->setFormat(Client::FORMAT_JSON);
        return $request;
    }

    /**
     * @param $request Request
     * @param $data
     * @return Response
     */
    public function getResponse($request, $data = null){
        $request->setMethod('get')->setData($data);

        $this->stdout("URL :: " . $request->getFullUrl() . PHP_EOL);
        return $request->send();
    }


    /**
     * @param $request Request
     * @param null $data
     * @return Response
     */
    public function postRequest($request, $data = null){
        $request->setData($data)->setMethod('post');
        $this->stdout("URL posted:: " . $request->getFullUrl() . PHP_EOL);
        return $request->send();
    }
}