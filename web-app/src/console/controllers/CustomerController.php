<?php
/**
 * Created by PhpStorm.
 * User: fahad
 * Date: 11/27/17
 * Time: 4:04 PM
 */
namespace console\controllers;

use yii\console\Controller;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use Faker\Factory;


class CustomerController extends BaseConsoleController
{
    /**
     * @return \yii\httpclient\Request
     */
    public function getRequest()
    {
        $request = parent::getRequest();
        $request->setUrl('customer');
        return $request;
    }

    /**
     *
     */
    public function actionIndex(){
        $request = $this->getRequest();
        $request->setMethod('get');
        $response = $request->send();
        VarDumper::dump($response->data);
    }

    /**
     *
     */
    public function actionCreate($max = 1){
        for($counter = 0; $counter < $max; $counter++){
            $customer = $this->getCustomer();
            $request = $this->getRequest();
            $request->setFormat(Client::FORMAT_JSON);
            $request->setUrl('customer')->setMethod('post');
            $request->setData($customer);

            $response = $request->send();
            VarDumper::dump($response->data);
        }
    }


    /**
     *
     */
    private function getCustomer(){
       $faker = Factory::create();
       $user = [];
       $user['firstName'] = $faker->firstName;
       $user['lastName'] = $faker->lastName;
       $user['email'] = $faker->email;
       $user['mobile'] = $faker->phoneNumber;
       $user['lang'] = 'en';
       return $user;
    }

}