<?php
/**
 * Created by PhpStorm.
 * User: damjad
 * Date: 12/5/17
 * Time: 5:05 PM
 */

namespace console\helpers;


use console\controllers\BaseConsoleController;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\httpclient\Request;
use yii\httpclient\Response;

class Fetcher
{
    /**
     * @return mixed
     */
    public static function services(){
        $services =  self::getList('services', Client::FORMAT_URLENCODED, []);
        return ArrayHelper::map($services, '_id', '_id');
    }

    /**
     * @return mixed
     */
    public static function customers(){
        return self::getList('customers', Client::FORMAT_URLENCODED, []);
    }

    /**
     * @return mixed
     */
    public static function contractors(){
        $response = self::getList('contractors', Client::FORMAT_URLENCODED, []);
        return $response['data'];
    }

    private static function getList($url, $format, $data){
        $request = self::getRequest();
        $request->setUrl($url)->setFormat($format);
        $response = self::getResponse($request, []);
        $services = $response->data;
        return $services;
    }


    public static function getClient(){
        $client = new Client(['baseUrl' => BaseConsoleController::API_ENDPOINT]);
        return $client;
    }

    /**
     * @return \yii\httpclient\Request
     */
    public static function getRequest(){
        $request = self::getClient()->createRequest();
        $request->setFormat(Client::FORMAT_JSON);
        return $request;
    }

    /**
     * @param $request Request
     * @param $data
     * @return Response
     */
    public static function getResponse($request, $data = null){
        $request->setMethod('get')->setData($data);
        Console::stdout("URL :: " . $request->getFullUrl() . PHP_EOL);
        return $request->send();
    }
}